Bonjour. 

Dans ce fichier, vous trouverez un petit tutoriel pour installer ce projet sur votre machine Linux en ligne de commande.

Tout d'abord, si vous lisez ces lignes, nous considérons que vous avez été invité à collaborer sur ce projet.

Vous devez donc possédez un compte Gitlab.

Placez vous dans un répértoire où vous souhaitez que le dépot github se trouve.

Ensuite, tapez cette commande pour clonner le dépot : 

git clone https://gitlab.com775pl/soanapi.git

Déplacez vous dans le répertoire : 

cd soanapi/

Crééz ensuite vote branche :

git branche <votrePseudo>

Vous êtes désormais prêt à contribuer au projet, félicitations !
